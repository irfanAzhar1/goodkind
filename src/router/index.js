import Vue from 'vue'
import VueRouter from 'vue-router'
import LeaderIndex from '../pages/leaders/index'
import LeaderDetail from '../pages/leaders/detail'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'leaderList',
    component: LeaderIndex
  },
  {
    path: '/:id',
    name: 'leaderDetail',
    component: LeaderDetail
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
