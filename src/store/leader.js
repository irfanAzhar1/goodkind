import { defineStore } from 'pinia'
import axios from "axios";

export const useLeader = defineStore('leader', {
  state: () => ({
    leadersList: [
    ],
    leaderDetail: {},
    pagination: {
      totalItem: 0,
      totalPage: 0,
      currentPage: 1
    },
    params: {
      q: '',
      size: 18,
    }
  }),
  getters: {
    getLeadersList(state) {
      return state.leadersList
    },
    getLeaderDetail(state) {
      return state.leaderDetail
    },
  },
  actions: {
    async fetchLeadersList() {
      const url = 'https://goodkind.id/api/leaders?page='+ this.pagination.currentPage + '&q=' + this.params.q+ '&size=18&withFilters=true&status=VERIFIED'
      try {
        const data = await axios.get(`https://api.allorigins.win/raw?url=${encodeURIComponent(url)}`)
        this.leadersList = data.data.data.map((leader) => {
          return {
            id: leader.id,
            name: leader.name,
            picUrl: leader.picThumbUrl,
          }
        })
        console.log(data.data.paging)
        this.pagination = {
          currentPage: parseInt(data.data.paging.page),
          totalPage: parseInt(data.data.paging.totalPage),
          totalItem: parseInt(data.data.paging.total)
        }
      } catch (err) {
        console.log(err)
      }
    }
  },
})
